%define release_version %(echo %{version} | awk -F. '{print $1"."$2}')

Name:           grilo
Version:        0.3.16
Release:        1
Summary:        A framework for browsing and searching media content
License:        LGPLv2+
URL:            https://wiki.gnome.org/Projects/Grilo
Source0:        https://download.gnome.org/sources/grilo/%{release_version}/grilo-%{version}.tar.xz

BuildRequires:  chrpath glib2-devel gettext gobject-introspection-devel >= 0.9.0
BuildRequires:  gtk-doc gtk3-devel liboauth-devel libsoup3-devel libxml2-devel
BuildRequires:  meson totem-pl-parser-devel vala >= 0.27.1 libxslt cmake

%description
Grilo is a framework focused on making media discovery and browsing easy for 
application developers.

More precisely, Grilo provides:

A single, high-level API that abstracts the differences among various media
content providers, allowing application developers to integrate content from
various services and sources easily.
A collection of plugins for accessing content from various media providers.
Developers can share efforts and code by writing plugins for the framework
that are application agnostic.
A flexible API that allows plugin developers to write plugins of various kinds.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
Libraries and header files are contained in %{name}-devel package. You need to
install the %{name}-devel package if you want to develop applications that use
%{name}.

%package_help

%prep
%autosetup -p1

%build
%meson -Denable-gtk-doc=true

%meson_build

%install
%meson_install
mkdir -p $RPM_BUILD_ROOT%{_libdir}/grilo-%{release_version}/
mkdir -p $RPM_BUILD_ROOT%{_datadir}/grilo-%{release_version}/plugins/
%find_lang grilo

%ldconfig_scriptlets

%files -f grilo.lang
%license COPYING
%{_bindir}/*
%{_libdir}/*.so.*
%{_libdir}/grilo-0.3/
%{_libdir}/girepository-1.0/
%{_datadir}/grilo-0.3/
%license COPYING

%files devel
%{_includedir}/grilo-0.3/
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/
%{_datadir}/vala/
%{_libdir}/*.so

%files help
%doc AUTHORS NEWS README.md TODO
%{_mandir}/man1/*.1*
%{_datadir}/gtk-doc/html/grilo/

%changelog
* Tue Jan 23 2024 zhouwenpei <zhouwenpei1@h-partners.com> - 0.3.16-1
- Update to 0.3.16

* Fri Nov 11 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.3.15-1
- Update to 0.3.15

* Fri May 20 2022 loong_C <loong_c@yeah.net> - 0.3.14-2
- fix spec changelog date

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 0.3.14-1
- Update to 0.3.14

* Mon Sep 13 2021 yangcheng<yangcheng87@huawei.com> - 0.3.13-2
- Type:CVE
- CVE:CVE-2021-39365
- SUG:NA
- DESC:fix CVE-2021-39365

* Tue Feb 2 2021 jinzhimin <jinzhimin2@huawei.com> - 0.3.13-1
- upgrade to 0.3.13

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.9-3
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Sat Nov 23 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.9-2
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:add the libxslt in buildrequires

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.3.9-1
- Package Init
